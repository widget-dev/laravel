<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductsFilterRequest;
use App\Http\Requests\SubscriptionRequest;
use App\Models\Category;
use App\Services\Widget\ClientService;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Sku;
use App\Models\Subscription;
use App\Services\Widget\DisplayService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use http\Cookie;
use Illuminate\Support\Str;

class MainController extends Controller
{
    /**
     * @var DisplayService
     */
    private $widgetDisplayService;

    /**
     * BasketController constructor.
     * @param DisplayService $widgetDisplayService
     */
    public function __construct(DisplayService $widgetDisplayService){
        $this->widgetDisplayService = $widgetDisplayService;
    }

    public function index(ProductsFilterRequest $request)
    {
        if(is_object(session('token'))){
            session(['token' => session('token')->token]);
        }
        $widgetDealsHtml = $this->widgetDisplayService->getWidgetDisplay(session('user_id'), session('token'));

		$skusQuery = Sku::with(['product', 'product.category']);

        if ($request->filled('price_from')) {
            $skusQuery->where('price', '>=', $request->price_from);
        }

        if ($request->filled('price_to')) {
            $skusQuery->where('price', '<=', $request->price_to);
        }

        foreach (['hit', 'new', 'recommend'] as $field) {
            if ($request->has($field)) {
                $skusQuery->whereHas('product', function ($query) use ($field) {
                    $query->$field();
                });
            }
        }

        $skus = $skusQuery->paginate(6)->withPath("?".$request->getQueryString());

        return view('index', compact('skus', 'widgetDealsHtml'));
    }

    public function categories()
    {
        return view('categories');
    }

    public function category($code)
    {
        if(is_object(session('token'))){
            session(['token' => session('token')->token]);
        }
        $widgetDealsHtml = $this->widgetDisplayService->getWidgetDisplay(session('user_id'),session('token'));
        $category = Category::where('code', $code)->first();
        return view('category', compact('category', 'widgetDealsHtml'));
    }

    public function sku($categoryCode, $productCode, Sku $skus)
    {
        if ($skus->product->code != $productCode) {
            abort(404, 'Product not found');
        }

        if ($skus->product->category->code != $categoryCode) {
            abort(404, 'Category not found');
        }

        if(is_object(session('token'))){
            session(['token' => session('token')->token]);
        }
        $widgetDealsHtml = $this->widgetDisplayService->getWidgetDisplay(session('user_id'),session('token'));

        return view('product', compact('skus', 'widgetDealsHtml'));
    }

    public function subscribe(SubscriptionRequest $request, Sku $skus)
    {
        Subscription::create([
            'email' => $request->email,
            'sku_id' => $skus->id,
        ]);

        return redirect()->back()->with('success', __('product.we_will_update'));
    }

    public function changeLocale($locale)
    {
        $availableLocales = ['ru', 'en'];
        if (!in_array($locale, $availableLocales)) {
            $locale = config('app.locale');
        }
        session(['locale' => $locale]);
        App::setLocale($locale);
        return redirect()->back();
    }

    public function changeCurrency($currencyCode)
    {
        $currency = Currency::byCode($currencyCode)->firstOrFail();
        session(['currency' => $currency->code]);
        return redirect()->back();
    }
}
