<?php

namespace App\Services\Widget;

use App\Services\Widget\ClientService;
use Illuminate\Http\JsonResponse;
use Carbon\Carbon;
use App\Models\Coupon;
use Illuminate\Http\Response;

class WidgetService
{
    public function get(
        ClientService $widgetDeals,
        string $code
    ):JsonResponse
    {
        $result = $widgetDeals->setCode($code)
            ->clickPromo()
            ->getResult();

        if(!$result['restOfTimeInMinutes']){
            return new JsonResponse(
                [
                    'status' => 'fail',
                    'message' => 'Не получен параметр restOfTimeInMinutes'
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        $coupon = Coupon::where('code','=', $code)
            ->first();

        if (!$coupon) {
            $createCouponResponse = [
                'code' => $result['code'],
                'only_once' => 1,
                'value' => $result['discount'],
                'type' => 2,
                'expired_at' => Carbon::now()->addMinute($result['restOfTimeInMinutes'])
            ];
            Coupon::create($createCouponResponse);
        }

        return new JsonResponse(
                $result,
                $widgetDeals->getResponseCode()
            );
    }

    public function prolongation(
        ClientService $widgetDeals,
        string $code
    ):JsonResponse
    {
        $result = $widgetDeals->setCode($code)
            ->prolongationPromo()
            ->getResult();

        return new JsonResponse(
            $result,
            $widgetDeals->getResponseCode()
        );
    }

    public function use(
        ClientService $widgetDeals,
        string $code
    ):JsonResponse
    {
        $result = $widgetDeals->setCode($code)
            ->usePromo()
            ->getResult();

        return new JsonResponse(
            $result,
            $widgetDeals->getResponseCode()
        );
    }
}
